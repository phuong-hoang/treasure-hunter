import * as PIXI from 'pixi.js';
class GameScene extends PIXI.Container{
    constructor() {
        super();
    }
    setVisible(visible){
        super.visible= visible;
    }
    addChild(child){
        super.addChild(child);
    }
}
export {GameScene};