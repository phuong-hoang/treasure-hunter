import { Sprite } from "./sprite";
import { keyboard } from "./supporFunction";
class Explorer extends Sprite {
    constructor(textTure) {
        super(textTure);
        this.left = keyboard("ArrowLeft");
        this.up = keyboard("ArrowUp");
        this.right = keyboard("ArrowRight");
        this.down = keyboard("ArrowDown");
    }
    control(){
    

        this.left.press = () => {
            this.vx = -3;
            if ( this.down.isDown) {
                this.vy = 3;
            } else if ( this.up.isDown) {
                this.vy = -3;
            }
          };
      
          this.left.release = () => {
              if (! this.right.isDown) {
                this.vx = 0;
              }
              if ( this.down.isUp &&  this.up.isUp) {
                this.vy = 0;
              }
          };
          
      
          //Up
          this.up.press = () => {
            this.vy = -3;
              if ( this.left.isDown ) {
                this.vx = -3;
               
              }else if ( this.right.isDown){
        
                this.vx = 3;
              }
          };
          this.up.release = () => {
            if (! this.down.isDown) {
                this.vy = 0;
            }
            if ( this.left.isUp &&  this.right.isUp) {
                this.vx = 0;
            }
          };
          
      
          //Right
          this.right.press = () => {
            this.vx = 3;
            if ( this.down.isDown) {
                this.vy = 3;
            } else if ( this.up.isDown) {
                this.vy = -3;
            }
          };
          this.right.release = () => {
              if (! this.left.isDown) {
                this.vx = 0;
              }
              if ( this.down.isUp &&  this.up.isUp) {
                this.vy = 0;
              }
          };
      
          //Down
          this.down.press = () => {
            this.vy = 3;
            if ( this.left.isDown) {
                this.vx = -3;
            } else if ( this.right.isDown) {
                this.vx = 3;
            }
          };
          this.down.release = () => {
              if (! this.up.isDown) {
                this.vy = 0;
              }
              if ( this.left.isUp &&  this.right.isUp) {
                this.vx = 0;
              }
          };
    }
}
export {Explorer}